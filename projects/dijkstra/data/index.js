

// Adjacency matrix

module.exports = [
  [Infinity, 1, Infinity, Infinity, 15, 2],
  [Infinity, Infinity, 2, Infinity, Infinity, 10],
  [Infinity, Infinity, Infinity, 10, Infinity, Infinity],
  [Infinity, Infinity, Infinity, Infinity, Infinity, Infinity],
  [Infinity, Infinity, Infinity, 4, Infinity, Infinity],
  [Infinity, Infinity, 3, 8, 3, Infinity]
];
