

class Vector2 {
  /**
   * @param {Number} x
   * @param {Number} y
   */
  constructor(x, y) {
    this[0] = x;
    this[1] = y;
  }
  /**
   * @param {Vector2|Array<Number>} v
   * @returns {boolean}
   */
  is(v) {
    return this[0] === v[0] && this[1] === v[1];
  }
  /**
   * @returns {Array<Number>}
   */
  toJSON() {
    return [this[0], this[1]];
  }
  /**
   * @returns {String}
   */
  toString() {
    return JSON.stringify(this.toJSON());
  }
}


module.exports = Vector2;