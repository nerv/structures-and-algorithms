

const Vector2 = require('./Vector2');


class Matrix {
  /**
   * @param {Array<Array<Number>>} arr
   */
  constructor(arr) {
    this.__m = arr;
  }
  /**
   * @param {Vector2|Array<Number>} vxFrom
   * @param {Vector2|Array<Number>} vxTo
   * @returns {Number|*}
   */
  getEdgeBetween(vxFrom, vxTo) {
    let m = this.__m;
    return m[vxFrom[0]][vxTo[1]];
  }
  /**
   * @param {Vector2|Array<Number>} vx
   * @returns {Array<Vector2>}
   */
  getSiblingsOf(vx) {
    let m = this.__m;
    let length = this.__m.length;
    let stack = [];
    for(let i = 0; i < length; i++) {
      if (m[vx[0]][i] > 0 && m[vx[0]][i] < Infinity) {
        stack.push(new Vector2(i, i));
      }
    }
    return stack;
  }
  /**
   * @returns {Array<Vector2>}
   */
  getAllVertexes() {
    let length = this.__m.length;
    let stack = [];
    for(let i = 0; i < length; i++) {
      stack.push(new Vector2(i, i));
    }
    return stack;
  }
  /**
   * @returns {Object}
   */
  toJSON() {
    return this.__m.toJSON();
  }
  /**
   * @returns {String}
   */
  toString() {
    return this.__m.toString();
  }
}


module.exports = Matrix;