

class PriorityQueue {
  /**
   * @param {Array<Vector2>} arr
   * @param {Map} map
   */
  constructor(arr = [], map) {
    this.__a = Array.from(arr);
    this.__m = map;
    this.__onSort = this.__onSort.bind(this);
  }
  /**
   * @returns {Vector2}
   */
  dequeue() {
    let a = this.__a;
    a.sort(this.__onSort);
    return this.__a.shift();
  }
  /**
   * @param {Vector2} vx1
   * @param {Vector2} vx2
   * @returns {Number}
   * @private
   */
  __onSort(vx1, vx2) {
    let m = this.__m;
    return m.get(vx1.toString()) - m.get(vx2.toString()); 
  }
  /**
   * @returns {Number}
   */
  get length() {
    return this.__a.length;
  }
}


module.exports = PriorityQueue;