

const Vector2 = require('./Vector2');
const Matrix = require('./Matrix');
const PriorityQueue = require('./PriorityQueue');


class Dijkstra {
  /**
   * @param {Matrix} m
   * @param {Array<Vector2>} vxs
   * @param {Array<Number>} tree
   * @param {Map} distances
   * @param {Vector2} vxFrom
   * @param {Vector2} [vxTo]
   */
  static evaluate(m, vxs, tree, distances, vxFrom, vxTo) {
    for(let vx of vxs) {
      distances.set(vx.toString(), Infinity);
    }

    distances.set(vxFrom.toString(), 0);

    let queue = new PriorityQueue(vxs, distances);
    let visited = new Set();

    // Calculation of distances
    while (queue.length) {
      let vx = queue.dequeue();

      let vxDistance = distances.get(vx.toString());

      for(let siblingVx of m.getSiblingsOf(vx)) {
        if (!visited.has(siblingVx.toString())) {
          let edgeDistance = m.getEdgeBetween(vx, siblingVx);
          let vxToSiblingVxDistance = finite(vxDistance) + edgeDistance;
          let siblingVxDistance = distances.get(siblingVx.toString());
          if (vxToSiblingVxDistance < siblingVxDistance) {
            distances.set(siblingVx.toString(), vxToSiblingVxDistance);
            tree[siblingVx[0]] = vx[0];
          }
        }
      }

      visited.add(vx.toString());
    }
  }
  /**
   * @param {Array<Number>} matrix An adjacency matrix
   * @param {Number} idxVxFrom An index of start vertex
   * @returns {Map}
   */
  static findShortestPaths(matrix, idxVxFrom) {
    let m = new Matrix(matrix);
    let vxFrom = new Vector2(idxVxFrom, idxVxFrom);
    let vxs = m.getAllVertexes();
    let distances = new Map();
    let tree = [];

    this.evaluate(m, vxs, tree, distances, vxFrom);

    return distances;
  }
  /**
   * @param {Array<Number>} matrix An adjacency matrix
   * @param {Number} idxVxFrom An index of start vertex
   * @param {Number} idxVxTo An index of end vertex
   * @returns {Array<Vector2>}
   */
  static findShortestPath(matrix, idxVxFrom, idxVxTo) {
    let m = new Matrix(matrix);
    let vxFrom = new Vector2(idxVxFrom, idxVxFrom);
    let vxTo = new Vector2(idxVxTo, idxVxTo);
    let vxs = m.getAllVertexes();
    let distances = new Map();
    let tree = [];

    this.evaluate(m, vxs, tree, distances, vxFrom, vxTo);

    // Restore shortest path
    let path = [];
    let vx = vxs[tree[vxTo[0]]];
    do {
      if (isUndefined(vx)) {
        return [];
      }
      path.unshift(vx);
      vx = vxs[tree[vx[0]]];
    } while (!vx.is(vxFrom));

    path.unshift(vxFrom);
    path.push(vxTo);

    return path;
  }
}

/**
 * @param {Number} n
 * @returns {Number}
 */
function finite(n) {
  return isFinite(n) ? n : 0;
}
/**
 * @param {*} any
 * @returns {Boolean}
 */
function isUndefined(any) {
  return undefined === any;
}

module.exports = Dijkstra;