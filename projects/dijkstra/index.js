

const Dijkstra = require('./algorithm/Dijkstra');
// Adjacency matrix
let matrix = require('./data');

let path = Dijkstra.findShortestPath(matrix, 0, 3);

console.log('path', path);