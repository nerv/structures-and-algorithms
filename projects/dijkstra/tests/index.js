

const {expect} = require('chai');
const Matrix = require('./../algorithm/Matrix');
const Dijkstra = require('./../algorithm/Dijkstra');
const data1 = require('./fixtures/data1');
const data2 = require('./fixtures/data2');


describe('Matrix', function () {
  const m = new Matrix(data1);
  describe('#getAllVertexes()', function () {
    it('Amount of vertexes should be equal to matrix.length', function () {
      let vxs = m.getAllVertexes();
      expect(vxs.length).to.equal(data1.length);
    });
    it('Vertexes should be valid', function () {
      let vxs = m.getAllVertexes();
      expect(String(vxs[0])).to.equal('[0,0]');
      expect(String(vxs[1])).to.equal('[1,1]');
      expect(String(vxs[2])).to.equal('[2,2]');
      expect(String(vxs[3])).to.equal('[3,3]');
      expect(String(vxs[4])).to.equal('[4,4]');
      expect(String(vxs[5])).to.equal('[5,5]');
    });
  });
  describe('#getSiblingsOf(vx)', function () {
    it('Siblings of 1 vx should be relevant', function () {
      let siblings = m.getSiblingsOf([0,0]);
      expect(String(siblings)).to.equal('[1,1],[4,4],[5,5]');
    });
    it('Siblings of 2 vx should be relevant', function () {
      let siblings = m.getSiblingsOf([1,1]);
      expect(String(siblings)).to.equal('[2,2],[5,5]');
    });
    it('Siblings of 3 vx should be relevant', function () {
      let siblings = m.getSiblingsOf([2,2]);
      expect(String(siblings)).to.equal('[3,3]');
    });
    it('Siblings of 4 vx should be relevant', function () {
      let siblings = m.getSiblingsOf([3,3]);
      expect(String(siblings)).to.equal('');
    });
    it('Siblings of 5 vx should be relevant', function () {
      let siblings = m.getSiblingsOf([4,4]);
      expect(String(siblings)).to.equal('[3,3]');
    });
    it('Siblings of 6 vx should be relevant', function () {
      let siblings = m.getSiblingsOf([5,5]);
      expect(String(siblings)).to.equal('[2,2],[3,3],[4,4]');
    });
  });
  describe('#getEdgeBetween(vxFrom, vxTo)', function () {
    it('An edge between 1vx and 2vx should be relevant', function () {
      let edge = m.getEdgeBetween([0,0],[1,1]);
      expect(edge).to.equal(1);
    });
    it('An edge between 2vx and 1vx should be relevant', function () {
      let edge = m.getEdgeBetween([1,1],[0,0]);
      expect(edge).to.equal(Infinity);
    });
    it('An edge between 1vx and 5vx should be relevant', function () {
      let edge = m.getEdgeBetween([0,0],[4,4]);
      expect(edge).to.equal(15);
    });
    it('An edge between 1vx and 6vx should be relevant', function () {
      let edge = m.getEdgeBetween([0,0],[5,5]);
      expect(edge).to.equal(2);
    });
    it('An edge between 4vx and 2vx should be relevant', function () {
      let edge = m.getEdgeBetween([3,3],[1,1]);
      expect(edge).to.equal(Infinity);
    });
    it('An edge between 6vx and 3vx should be relevant', function () {
      let edge = m.getEdgeBetween([5,5],[2,2]);
      expect(edge).to.equal(3);
    });
    it('An edge between 2vx and 6vx should be relevant', function () {
      let edge = m.getEdgeBetween([1,1],[5,5]);
      expect(edge).to.equal(10);
    });
  });
});


describe('Dijkstra', function () {
  describe('#findShortestPath(matrix, vxIdxFrom, vxIdxTo)', function () {
    it('Path from 1vx to 4vx should be shortest', function () {
      let path = Dijkstra.findShortestPath(data1, 0, 3);
      expect(String(path)).to.equal('[0,0],[5,5],[4,4],[3,3]');
    });
    it('Path from 4vx to 1vx should be shortest', function () {
      let path = Dijkstra.findShortestPath(data1, 3, 0);
      expect(String(path)).to.equal('');
    });
    it('Path from 4vx to 1vx should be shortest', function () {
      let path = Dijkstra.findShortestPath(data2, 0, 4);
      expect(String(path)).to.equal('[0,0],[1,1],[3,3],[2,2],[5,5],[4,4]');
    });
  });
});