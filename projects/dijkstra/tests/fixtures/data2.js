

// Adjacency matrix

module.exports = [
  [Infinity, 1, Infinity, Infinity, Infinity, Infinity],
  [Infinity, Infinity, 5, 2, Infinity, 7],
  [Infinity, Infinity, Infinity, 1, Infinity, 1],
  [2, Infinity, 1, Infinity, 4, Infinity],
  [Infinity, Infinity, Infinity, 4, Infinity, Infinity],
  [Infinity, Infinity, Infinity, Infinity, 1, Infinity]
];
