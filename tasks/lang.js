'use strict';

const {stringify} = JSON;

let {isArray} = Array;
/**
 * 
 * @param {*} any
 * @returns {String}
 */
function toString(any) {
  if (isObject(any)) {
    if (isMatrix(any)) {
      return toStringMatrix(any);
    } else {
      return stringify(any);
    }
  } else {
    return String(any);
  }
}
/**
 * @param {Array<Array>} matrix
 * @returns {String}
 */
function toStringMatrix(matrix) {
  let rowsCount = matrix.length;
  let lines = [];
  for(let i = 0; i < rowsCount; i++) {
    lines.push('[' + String(matrix[i]) + ']');
  }
  return lines.join('\n');
}
/**
 * @param {*} any
 * @returns {Boolean}
 */
function isObject(any) {
  return any !== null && typeof any === 'object';
}
/**
 * @param {*} any
 * @returns {Boolean}
 */
function isMatrix(any) {
  return isArray(any) && isArray(any[0]);
}
/**
 * @param {Number} size
 * @returns {Array<Array>}
 */
function createZeroMatrix(size) {
  let matrix = [];
  for(let i = 0; i < size; i++) {
    let row = new Array(size);
    row.fill(0);
    matrix.push(row);
  }
  return matrix;
}

exports.toString = toString;
exports.createZeroMatrix = createZeroMatrix;