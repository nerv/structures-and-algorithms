'use strict';

const {createZeroMatrix} = require('./../lang');


class Structure {
  /**
   * @param {Array<Array>} matrix
   * @returns {Structure}
   */
  constructor(matrix) {
    this.al = [];
    this.j = [];
    this.pack(matrix);
  }
  /**
   * @param {Array<Array>} matrix
   * @returns {Structure}
   */
  pack(matrix) {
    let rowsCount = matrix.length;
    let columnsCount = matrix[0].length;
    for(let j = 0; j < columnsCount; j++) {
      this.al.push(0);
      this.j.push(j + 1);
      for(let i = 0; i < rowsCount; i++) {
        let n = matrix[i][j];
        if (n > 0) {
          this.al.push(n);
          this.j.push(i + 1);
        }
      }
    }
    this.al.push(0);
    this.j.push(0);
    return this;
  }
  /**
   * @param {Structure} structure
   * @returns {Array<Array>}
   */
  unpack(structure = this) {
    let st = structure;
    let count = st.al.length;
    let size = this.getMatrixSize();
    let matrix = createZeroMatrix(size);
    for(let k = 0, j = -1; k < count; k++) {
      if (st.al[k] === 0) {
        j += 1;
      }
      if (st.al[k] > 0 && st.j[k] > 0) {
        let i = st.j[k] - 1;
        let val = st.al[k];
        matrix[i][j] = val;
      }
    }
    return matrix;
  }
  /**
   * @returns {Number}
   */
  getMatrixSize() {
    return this.al.reduce((sum, n) => n === 0 ? sum + 1 : sum, -1);
  }
  /**
   * @param {Array<Array>} matrix
   * @returns {Structure}
   */
  static pack(matrix) {
    return new Structure(matrix);
  }
  /**
   * @param {Structure} structure
   * @returns {Array<Array>}
   */
  static unpack(structure) {
    return structure.unpack();
  }
}

/*
 Вариант 3. Структура смежности по столбцам формируется анало-
 гично, но ненулевые элементы списка AL располагаются по столбцам ис-
 ходной матрицы. Элементы списка I, соответствующие нулям списка AL,
 определяют номера столбцов, описания которых начинаются со следую-
 щих позиций списков AL и I, а остальные элементы списка I соответ-
 ствуют номерам строк ненулевых элементов текущего столбца
 */
let sample = {
  matrix: [
    [0,5,2,0,0],
    [0,0,7,0,0],
    [0,4,0,6,1],
    [0,3,0,0,9],
    [0,0,0,0,0]
  ],
  structure: {
    al: [0,0,5,4,3,0,2,7,0,6,0,1,9,0],
    j: [1,2,1,3,4,3,1,2,4,3,5,3,4,0]
  }
};

Structure.sample = sample;

module.exports = Structure;