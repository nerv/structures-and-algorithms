'use strict';

const {toString} = require('./../lang');
const Structure = require('./variant_5');

let structure = Structure.pack(Structure.sample.matrix);
let matrix = Structure.unpack(structure);

console.log('A sample of structure:');
console.log(toString(Structure.sample.structure));
console.log('A result of packing:');
console.log(toString(structure));
console.assert(toString(Structure.sample.structure) === toString(structure));

console.log('A sample of matrix:');
console.log(toString(Structure.sample.matrix));
console.log('A result of unpacking:');
console.log(toString(matrix));
console.assert(toString(Structure.sample.matrix) === toString(matrix));