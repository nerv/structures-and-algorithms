'use strict';

const {createZeroMatrix} = require('./../lang');


class Structure {
  /**
   * @param {Array<Array>} matrix
   * @returns {Structure}
   */
  constructor(matrix) {
    this.al = [];
    this.i = [];
    this.jc = [];
    this.pack(matrix);
  }
  /**
   * @param {Array<Array>} matrix
   * @returns {Structure}
   */
  pack(matrix) {
    let rowsCount = matrix.length;
    let columnsCount = matrix[0].length;
    let k = 0;
    this.jc.push(k);
    for(let j = 0; j < columnsCount; j++) {
      for(let i = 0; i < rowsCount; i++) {
        let n = matrix[i][j];
        if (n > 0) {
          this.al.push(n);
          this.i.push(i + 1);
          k += 1;
        }
      }
      this.jc.push(k);
    }
    return this;
  }
  /**
   * @param {Structure} structure
   * @returns {Array<Array>}
   */
  unpack(structure = this) {
    let st = structure;
    let count = st.jc.length - 1;
    let size = this.getMatrixSize();
    let matrix = createZeroMatrix(size);
    for(let j = 0; j < count; j++) {
      let startJc = st.jc[j];
      let endJc = st.jc[j + 1];
      for(let jc = startJc; jc < endJc; jc++) {
        let i = st.i[jc] - 1;
        let val = st.al[jc];
        matrix[i][j] = val;
      }
    }
    return matrix;
  }
  /**
   * @returns {Number}
   */
  getMatrixSize() {
    return this.jc.length - 1;
  }
  /**
   * @param {Array<Array>} matrix
   * @returns {Structure}
   */
  static pack(matrix) {
    return new Structure(matrix);
  }
  /**
   * @param {Structure} structure
   * @returns {Array<Array>}
   */
  static unpack(structure) {
    return structure.unpack();
  }
}

/*
 Вариант 5. Столбцовая схема упаковки реализуется аналогичным
 образом, с той лишь разницей, что формирование списков, входящих в
 структуру смежности, происходит по столбцам исходной матрицы.
 В этом случае список I определяет строчные индексы ненулевых
 элементов матрицы, а список JC – позиции в списках AL и I, на которых
 оканчивается описание ненулевых элементов очередного столбца. Следо-
 вательно, если j-й столбец содержит только нули, то JC[j-1]= JC[j]. В
 противном случае списки AL и I содержат описание ненулевых элементов
 j-го столбца в позициях от JC[j-1]+1 до JC[j].
 */
let sample = {
  matrix: [
    [0,5,2,0,0],
    [0,0,7,0,0],
    [0,4,0,6,1],
    [0,3,0,0,9],
    [0,0,0,0,0]
  ],
  structure: {
    al: [5,4,3,2,7,6,1,9],
    i: [1,3,4,1,2,3,3,4],
    jc: [0,0,3,5,6,8]
  }
};

Structure.sample = sample;

module.exports = Structure;