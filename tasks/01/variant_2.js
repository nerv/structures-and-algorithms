'use strict';

const {createZeroMatrix} = require('./../lang');


class Structure {
  /**
   * @param {Array<Array>} matrix
   * @returns {Structure}
   */
  constructor(matrix) {
    this.al = [];
    this.j = [];
    this.pack(matrix);
  }
  /**
   * @param {Array<Array>} matrix
   * @returns {Structure}
   */
  pack(matrix) {
    let rowsCount = matrix.length;
    let columnsCount = matrix[0].length;
    for(let i = 0; i < rowsCount; i++) {
      this.al.push(0);
      this.j.push(i + 1);
      for(let j = 0; j < columnsCount; j++) {
        let n = matrix[i][j];
        if (n > 0) {
          this.al.push(n);
          this.j.push(j + 1);
        }
      }
    }
    this.al.push(0);
    this.j.push(0);
    return this;
  }
  /**
   * @param {Structure} structure
   * @returns {Array<Array>}
   */
  unpack(structure = this) {
    let st = structure;
    let count = st.al.length;
    let size = this.getMatrixSize();
    let matrix = createZeroMatrix(size);
    for(let k = 0, i = -1; k < count; k++) {
      if (st.al[k] === 0) {
        i += 1;
      }
      if (st.al[k] > 0 && st.j[k] > 0) {
        let j = st.j[k] - 1;
        let val = st.al[k];
        matrix[i][j] = val;
      }
    }
    return matrix;
  }
  /**
   * @returns {Number}
   */
  getMatrixSize() {
    return this.al.reduce((sum, n) => n === 0 ? sum + 1 : sum, -1);
  }
  /**
   * @param {Array<Array>} matrix
   * @returns {Structure}
   */
  static pack(matrix) {
    return new Structure(matrix);
  }
  /**
   * @param {Structure} structure
   * @returns {Array<Array>}
   */
  static unpack(structure) {
    return structure.unpack();
  }
}

/*
 Вариант 2. Структура смежности по строкам состоит из двух не-
 связных списков. Список AL содержит ненулевые элементы матрицы, пе-
 речисленные по строкам, а элементы на соответствующих позициях второ-
 го списка J определяют номера столбцов ненулевых элементов из списка
 AL. Нуль в списке AL означает начало описания ненулевых элементов оче-
 редной строки матрицы. Нулю списка AL соответствует элемент списка J,
 определяющий номер строки, описание которой начинается со следующей
 позиции списков AL и J. Нули на одной позиции списков AL и J указыва-
 ют на конец упакованной формы.
 */
let sample = {
  matrix: [
    [0,5,2,0,0],
    [0,0,7,0,0],
    [0,4,0,6,1],
    [0,3,0,0,9],
    [0,0,0,0,0]
  ],
  structure: {
    al: [0,5,2,0,7,0,4,6,1,0,3,9,0,0],
    j: [1,2,3,2,3,3,2,4,5,4,2,5,5,0]
  }
};

Structure.sample = sample;

module.exports = Structure;